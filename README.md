# react-antd-admin

#### 介绍
本项目使用react  typescript vite 搭建的后台管理模板，已经实现权限模块  
[演示地址](http://152.136.150.148/home)
账号：admin 
密码：123456
#### 软件架构
React 17 + React-Router + React-Redux + Axios + Typescript + Vite


#### 安装教程

1.  yarn install
2.  yarn dev   开发环境
3.  yarn build  打包
4.  yarn serve   预览


