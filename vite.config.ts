import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { minifyHtml, injectHtml } from "vite-plugin-html";
import path from "path";
import { createStyleImportPlugin, AntdResolve } from "vite-plugin-style-import";
import legacy from "@vitejs/plugin-legacy";
export default defineConfig({
    plugins: [
        createStyleImportPlugin({
            resolves: [AntdResolve()],
        }),
        react(),
        legacy({
            targets: ["defaults", "not IE 11"],
        }),
        minifyHtml(),
        injectHtml({
            data: {
                title: "react-antd-admin",
                injectScript: "",
            },
        }),
    ],
    css: {
        modules: {
            localsConvention: "camelCase",
        },

        preprocessorOptions: {
            scss: {
                additionalData: '@import "@/style/variable.scss";',
            },
            less: {
                // 支持内联 javascript
                javascriptEnabled: true,
            },
        },
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
    },
    server: {
        proxy: {
            "/api": {
                target: "http://152.136.150.148",
                changeOrigin: true,
            },
            "/uploads": {
                target: "http://localhost:3001",
                changeOrigin: true,
            },
        },
    },
    build: {
        sourcemap: true,
    },
});
