export const dataStatusOptions = [
    {
        value: 1,
        label: "启用",
    },
    {
        value: 0,
        label: "停用",
    },
] as const;
