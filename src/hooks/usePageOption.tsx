import { Dispatch, SetStateAction, useState } from "react";
import { TablePaginationConfig } from "antd";
const usePageOption = (
    onChange: (page: number, pageSize?: number) => void
): [TablePaginationConfig, Dispatch<SetStateAction<TablePaginationConfig>>] => {
    const [pageOption, setPageOption]: [
        TablePaginationConfig,
        Dispatch<SetStateAction<TablePaginationConfig>>
    ] = useState<TablePaginationConfig>({
        showSizeChanger: true,
        total: 0,
        pageSize: 10,
        onChange: (page, pageSize) => {
            onChange(page, pageSize);
            pageSize && setPageOption({ ...pageOption, pageSize });
        },
        showTotal: (total, range) => {
            return <span>共计{total}条</span>;
        },
    });
    return [pageOption, setPageOption];
};
export default usePageOption;
