import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Layout from "@/views/layout/index.entry";
import React, { Suspense, useEffect } from "react";
import { IRoute } from "@/types";
import { useMySelector } from "@/redux/hook";
import { useDispatch } from "react-redux";
import { generateRoutes } from "@/redux/menus/menuReducer";
import { generateAffixTag } from "@/redux/tagView/tagViewReducer";
export const Routers: IRoute[] = [
    {
        path: "/login",
        name: "登录",
        component: "login",
        exact: true,
        auth: "noAuth",
        meta: {
            hidden: true,
        },
    },
    {
        path: "/home",
        name: "Home",
        component: "home/home",
        auth: "auth",
        meta: {
            title: "首页",
            layout: true,
            icon: "icon-shouye",
            isAffix: true,
        },
    },
];
const Routes = () => {
    //const  [routes,setRoutes]  = useState<IRoute[]>([]);
    const routes = useMySelector(state => state.menu.routes);
    const token = useMySelector(state => state.user.token);
    const dispatch = useDispatch();
    const buildRoutes = (routes: IRoute[]) => {
        return routes.map(r => {
            const Component = () => (
                <r.component>
                    {r.children && <Switch>{buildRoutes(r.children)}</Switch>}
                </r.component>
            );
            const generateRote = () => {
                const CRoute = () => (
                    <Route
                        exact={r.exact}
                        key={r.name}
                        path={r.path}
                        component={() =>
                            r.meta?.layout ? (
                                <Layout>
                                    <Component />
                                </Layout>
                            ) : (
                                <Component />
                            )
                        }
                    />
                );
                if (r.auth === "auth") {
                    return !token ? (
                        <Redirect key={r.name} from={r.path} to={"/login"} />
                    ) : (
                        CRoute()
                    );
                } else if (r.auth == "noAuth") {
                    return token ? (
                        <Redirect key={r.name} from={r.path} to={"/home"} />
                    ) : (
                        CRoute()
                    );
                } else {
                    return CRoute();
                }
            };
            return r.redirect ? (
                <Redirect
                    key={r.name}
                    exact={r.exact}
                    from={r.redirect.path}
                    to={r.redirect.to}
                />
            ) : (
                generateRote()
            );
        });
    };
    useEffect(() => {
        dispatch(generateRoutes());
    }, []);
    return (
        <BrowserRouter>
            <Switch>{buildRoutes(routes)}</Switch>
        </BrowserRouter>
    );
};
export default Routes;
