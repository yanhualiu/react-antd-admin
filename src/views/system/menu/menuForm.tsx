import {
    Col,
    Form,
    Input,
    InputNumber,
    Modal,
    Row,
    Select,
    Switch,
    TreeSelect,
} from "antd";
import React, { useEffect, useState } from "react";
import { Menu, MenuForm } from "@/types";
const { TreeNode } = TreeSelect;

interface MenuFormProps {
    title?: string;
    visible: boolean;
    submit: (menuForm: MenuForm) => void;
    handleCancel: () => void;
    menuTree: Menu[];
    editForm?: MenuForm;
    parentId?: number;
}

const scopeOptions = ["auth", "noAuth", "public"];
const MenuFormModal: React.FC<MenuFormProps> = ({
    title = "新建",
    visible,
    submit,
    handleCancel,
    menuTree,
    editForm,
    parentId,
}) => {
    const initForm = () => {
        return {
            name: "",
            path: "",
            permission: "",
            sort: 0,
            type: 0,
            parentId: parentId,
            component: "",
            icon: "",
            scope: "auth",
            layout: false,
            hidden: false,
        };
    };
    const [menuForm, setMenuForm] = useState<MenuForm>(editForm ?? initForm());
    const [form] = Form.useForm();
    const handleOk = async () => {
        const res = await form.validateFields();
        const values = form.getFieldsValue();
        console.log(values);
        submit(values);
    };
    const cancel = () => {
        handleCancel();
    };
    const buildTreeNode = (menus: Menu[]) => {
        return menus.map(m => {
            return (
                <TreeNode value={m.id} key={m.id} title={m.name}>
                    {m.children && buildTreeNode(m.children)}
                </TreeNode>
            );
        });
    };
    useEffect(() => {
        if (!visible) {
            setMenuForm(initForm());
        } else {
            setMenuForm(editForm ?? initForm());
            form.setFieldsValue(editForm ?? initForm());
        }
    }, [visible]);
    return (
        <Modal
            title={editForm ? "编辑" : "新建"}
            visible={visible}
            onOk={handleOk}
            destroyOnClose
            onCancel={cancel}
        >
            <Form form={form} labelCol={{ span: 5 }}>
                <Form.Item
                    label="上级菜单"
                    key="parentId"
                    name="parentId"
                    required
                >
                    <TreeSelect
                        showSearch
                        value={menuForm.parentId}
                        onChange={v =>
                            setMenuForm({ ...menuForm, parentId: v })
                        }
                    >
                        {buildTreeNode(menuTree)}
                    </TreeSelect>
                </Form.Item>
                <Form.Item
                    label="菜单名称"
                    key="name"
                    name="name"
                    rules={[
                        { required: true, message: "请填写菜单名称" },
                        { max: 20, message: "菜单名称不能超过20个字符" },
                    ]}
                >
                    <Input
                        value={menuForm.name}
                        onChange={e =>
                            setMenuForm({ ...menuForm, name: e.target.value })
                        }
                    />
                </Form.Item>
                <Form.Item
                    label="菜单路径"
                    key="path"
                    name="path"
                    rules={[
                        { required: true, message: "请填写菜单路径" },
                        { max: 50, message: "菜单路径不能超过50个字符" },
                    ]}
                >
                    <Input
                        value={menuForm.path}
                        onChange={e =>
                            setMenuForm({ ...menuForm, path: e.target.value })
                        }
                    />
                </Form.Item>
                <Form.Item
                    label="组件地址"
                    key="component"
                    name="component"
                    rules={[
                        { required: true, message: "请填写组件地址" },
                        { max: 100, message: "组件地址不能超过100个字符" },
                    ]}
                >
                    <Input
                        value={menuForm.component}
                        onChange={e =>
                            setMenuForm({
                                ...menuForm,
                                component: e.target.value,
                            })
                        }
                    />
                </Form.Item>
                <Form.Item
                    label="菜单图标"
                    key="icon"
                    name="icon"
                    rules={[{ max: 50, message: "菜单图标不能超过15个字符" }]}
                >
                    <Input
                        value={menuForm.icon}
                        onChange={e =>
                            setMenuForm({ ...menuForm, icon: e.target.value })
                        }
                    />
                </Form.Item>
                <Row>
                    <Col span={12} key="0">
                        <Form.Item label="layout" key="layout" name="layout">
                            <Switch
                                defaultChecked={menuForm.layout}
                                checked={menuForm.layout}
                                onChange={v =>
                                    setMenuForm({ ...menuForm, layout: v })
                                }
                                checkedChildren="是"
                                unCheckedChildren="否"
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12} key={1}>
                        <Form.Item label="hidden" key="hidden" name="hidden">
                            <Switch
                                defaultChecked={menuForm.hidden}
                                checked={menuForm.hidden}
                                checkedChildren="是"
                                unCheckedChildren="否"
                                onChange={v =>
                                    setMenuForm({ ...menuForm, hidden: v })
                                }
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={12} key={2}>
                        <Form.Item label="scope" key="scope" name="scope">
                            <Select
                                value={menuForm.scope}
                                onChange={v =>
                                    setMenuForm({ ...menuForm, scope: v })
                                }
                            >
                                {scopeOptions.map(s => (
                                    <Select.Option value={s} key={s}>
                                        {s}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item
                            label="排序"
                            name="sort"
                            labelCol={{ span: 6 }}
                            rules={[
                                { required: true, message: "排序不能为空" },
                            ]}
                        >
                            <InputNumber
                                value={menuForm.sort}
                                min={0}
                                onChange={v =>
                                    setMenuForm({ ...menuForm, sort: v })
                                }
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    );
};
export default MenuFormModal;
