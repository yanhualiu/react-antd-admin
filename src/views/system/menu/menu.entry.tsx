import { Button, Col, message, Row, Table, Popconfirm, Input } from "antd";
import { addMenu, delMenu, getMenuTree, updateMenu } from "@/api";
import React, { useEffect, useState } from "react";
import { Menu, MenuForm } from "@/types";
import MenuFormModal from "./menuForm";
const { Search } = Input;
const MenuPage: React.FC = () => {
    const columns = [
        {
            title: "菜单名称",
            dataIndex: "name",
            key: "name",
            width: "300px",
        },
        {
            title: "菜单路径",
            dataIndex: "path",
            key: "path",
        },
        {
            title: "菜单图标",
            dataIndex: "icon",
            key: "icon",
        },
        {
            title: "菜单排序",
            dataIndex: "sort",
            key: "sort",
        },
        {
            title: "操作",
            key: "handle",
            width: "200px",
            render(row: Menu) {
                return (
                    <>
                        <Button
                            type="link"
                            size="small"
                            onClick={() => {
                                setParentId(row.id);
                                setVisible(true);
                            }}
                        >
                            新建
                        </Button>
                        <Button
                            type="link"
                            size="small"
                            onClick={() => {
                                setEditForm(row);
                                setVisible(true);
                            }}
                        >
                            编辑
                        </Button>
                        <Popconfirm
                            title="是否要删除当前部门"
                            onConfirm={() => deleteMenu(row.id)}
                            okText="确定"
                            cancelText="取消"
                        >
                            <Button type="link" size="small">
                                删除
                            </Button>
                        </Popconfirm>
                    </>
                );
            },
        },
    ];
    const [menus, setMenus] = useState<Menu[]>([]);
    const [visible, setVisible] = useState(false);
    const [editForm, setEditForm] = useState<MenuForm | undefined>(undefined);
    const [parentId, setParentId] = useState(0);
    const getMenuList = async (menuName?: string) => {
        const res = await getMenuTree({ menuName });
        setMenus(res.data);
    };
    const onSearch = (val: string) => {
        getMenuList(val);
    };
    const [submitFlag, setSubmitFlag] = useState(false);
    const submit = async (form: MenuForm) => {
        if (submitFlag) {
            return;
        }
        setSubmitFlag(true);
        if (editForm) {
            form.id = editForm.id;
            try {
                const res = await updateMenu(form);
                message.success(res.message);
                setVisible(false);
                getMenuList();
            } finally {
                setSubmitFlag(false);
            }
        } else {
            try {
                const res = await addMenu(form);
                message.success(res.message);
                setVisible(false);
                getMenuList();
            } finally {
                setSubmitFlag(false);
            }
        }
    };
    const deleteMenu = (id: number) => {
        delMenu(id).then(() => {
            message.success("删除成功");
            getMenuList();
        });
    };
    useEffect(() => {
        getMenuList();
    }, []);
    useEffect(() => {
        if (!visible) {
            setParentId(0);
        }
    }, [visible]);
    return (
        <div>
            <div>
                <Row>
                    <Col span={8}>
                        <Button onClick={() => setVisible(true)}>新建</Button>
                    </Col>
                    <Col span={8} offset={8}>
                        <Search
                            placeholder="菜单名称"
                            size={"large"}
                            onSearch={onSearch}
                            enterButton
                        />
                    </Col>
                </Row>
            </div>
            <div style={{ marginTop: 10 }}>
                <Table
                    columns={columns}
                    bordered
                    dataSource={menus}
                    rowKey={"id"}
                    pagination={false}
                />
            </div>
            <MenuFormModal
                visible={visible}
                submit={submit}
                editForm={editForm}
                parentId={parentId}
                handleCancel={() => {
                    setEditForm(undefined);
                    setVisible(false);
                }}
                menuTree={menus}
            />
        </div>
    );
};
export default MenuPage;
