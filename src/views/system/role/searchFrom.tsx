import { Form, Input, Select, DatePicker, Button } from "antd";
import { useState } from "react";
import { dataStatusOptions } from "../../../utils/utils";
interface SearchForm {
    roleName: string;
    roleDesc: string;
    dataStatus?: number;
    createTimeRange: string[];
}
interface RoleSearchProps {
    onSearch: (searchForm: SearchForm) => void;
    onRest: (searchForm: SearchForm) => void;
}
const RoleSearch: React.FC<RoleSearchProps> = props => {
    const [searchForm, setSearchForm] = useState<SearchForm>({
        roleName: "",
        roleDesc: "",
        dataStatus: undefined,
        createTimeRange: [],
    });
    return (
        <div>
            <Form layout="inline" onFinish={() => props.onSearch(searchForm)}>
                <Form.Item label="角色编码" name="roleName">
                    <Input
                        value={searchForm.roleName}
                        onChange={e =>
                            setSearchForm({
                                ...searchForm,
                                roleName: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item label="角色名" name="roleDesc">
                    <Input
                        value={searchForm.roleDesc}
                        onChange={e =>
                            setSearchForm({
                                ...searchForm,
                                roleDesc: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item label="状态" name="dataStatus">
                    <Select
                        placeholder="请选择状态"
                        style={{ width: 150 }}
                        allowClear
                        onChange={v =>
                            setSearchForm({
                                ...searchForm,
                                dataStatus: v ? (v as number) : (v as any),
                            })
                        }
                    >
                        {dataStatusOptions.map(d => (
                            <Select.Option key={d.value} value={d.value}>
                                {d.label}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="创建时间" name="createTimeRange">
                    <DatePicker.RangePicker
                        onChange={(dates, dateStrings) =>
                            setSearchForm({
                                ...searchForm,
                                createTimeRange: dateStrings,
                            })
                        }
                    ></DatePicker.RangePicker>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        搜索
                    </Button>
                </Form.Item>
                <Form.Item>
                    <Button
                        type="default"
                        htmlType="reset"
                        onClick={() => {
                            setSearchForm({
                                roleName: "",
                                roleDesc: "",
                                dataStatus: undefined,
                                createTimeRange: [],
                            });
                            props.onRest(searchForm);
                        }}
                    >
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};
export default RoleSearch;
