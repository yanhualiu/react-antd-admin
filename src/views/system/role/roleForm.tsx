import { Menu, RoleForm, Role } from "@/types";
import { Modal, Form, Input, Tree } from "antd";
import { DataNode } from "rc-tree/lib/interface";
import React, { useState } from "react";
import { useEffect } from "react";
import { getMenuTree } from "../../../api/menu/index";
import _ from "lodash";
const buildTreeData = (datas: Menu[]): DataNode[] => {
    return datas.map(d => {
        let newD: any = {
            key: d.id,
            title: d.name,
        };
        if (d.children) {
            newD.children = buildTreeData(d.children);
        }
        return newD;
    });
};
interface RoleFormModalProps {
    visible: boolean;
    onCancel: () => void;
    submit: (form: RoleForm) => void;
    editRole?: Role;
}
const getChildren = (parentId: number, menus: Menu[]) => {
    const children = [];
    for (let i = 0; i < menus.length; i++) {
        const menu = menus[i];
        if (menu.parentId === parentId) {
            children.push(menu);
            menus.splice(i, 1);
            i--;
        }
    }
    return children;
};
const RoleFormModal: React.FC<RoleFormModalProps> = props => {
    const cancel = () => {
        props.onCancel();
    };
    const [menusTree, setMenusTree] = useState<DataNode[]>([]);
    const [form] = Form.useForm();
    const [roleForm, setRoleForm] = useState<RoleForm>({
        roleName: "",
        roleDesc: "",
        menus: [],
    });
    const [defaultCheckKeys, setDefaultCheckKeys] = useState<number[]>([]);
    const onCheck = (checkedKeys: any, e: any) => {
        setRoleForm({
            ...roleForm,
            menus: checkedKeys.concat(e.halfCheckedKeys),
        });
    };
    const onOk = async () => {
        await form.validateFields();
        const values = form.getFieldsValue();
        props.submit({ ...values, menus: roleForm.menus });
    };
    const initDefaultCheckKeys = (menus: Menu[]) => {
        const copyMenu = _.cloneDeep(menus);
        const root = getChildren(0, copyMenu!);
        const buildMenus = (parent: Menu[]) => {
            parent.forEach(m => {
                const children = getChildren(m.id, copyMenu!);
                if (children) {
                    m.children = children;
                    buildMenus(children);
                }
            });
        };
        buildMenus(root);
        const delArr: number[] = [];
        const deepRangeMenus = (menus: Menu[]) => {
            menus.forEach(rm => {
                const menuTreeitem = menusTree.find(d => d.key === rm.id);
                if (
                    menuTreeitem?.children?.length! > 0 &&
                    rm.children?.length !== menuTreeitem?.children?.length
                ) {
                    delArr.push(rm.id);
                }
                if (rm.children) {
                    deepRangeMenus(rm.children);
                }
            });
        };
        deepRangeMenus(root);
        const defaultKeys = props.editRole?.menus?.map(d => d.id) ?? [];
        if (delArr.length > 0) {
            for (let i = 0; i < defaultKeys.length; i++) {
                const val = defaultKeys[i];
                if (delArr.includes(val)) {
                    defaultKeys.splice(i, 1);
                    i--;
                }
            }
        }
        setDefaultCheckKeys(defaultKeys);
    };
    useEffect(() => {
        const editRole = props.editRole;
        if (editRole) {
            const menus = editRole.menus?.map(m => m.id);
            setRoleForm({
                id: editRole.id,
                roleName: editRole.roleName,
                roleDesc: editRole.roleDesc,
                menus: menus as number[],
            });
            form.setFieldsValue({
                roleName: editRole.roleName,
                roleDesc: editRole.roleDesc,
            });
            initDefaultCheckKeys(editRole.menus!);
        }
    }, [props.editRole]);
    useEffect(() => {
        getMenuTree().then(res => {
            const data = buildTreeData(res.data);
            setMenusTree(data);
        });
    }, []);
    return (
        <Modal
            visible={props.visible}
            destroyOnClose
            title="表单"
            onCancel={cancel}
            onOk={onOk}
        >
            <Form form={form}>
                <Form.Item
                    label="角色编码"
                    name="roleName"
                    rules={[{ required: true, message: "请填写角色编码" }]}
                >
                    <Input
                        value={roleForm.roleName}
                        onChange={e =>
                            setRoleForm({
                                ...roleForm,
                                roleName: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item
                    label="角色名称"
                    name="roleDesc"
                    rules={[{ required: true, message: "请填写角色名称" }]}
                >
                    <Input
                        value={roleForm.roleDesc}
                        onChange={e =>
                            setRoleForm({
                                ...roleForm,
                                roleDesc: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item label="关联菜单">
                    <Tree
                        checkable
                        blockNode
                        defaultExpandAll
                        defaultCheckedKeys={defaultCheckKeys}
                        onCheck={onCheck}
                        treeData={menusTree}
                    ></Tree>
                </Form.Item>
            </Form>
        </Modal>
    );
};
export default RoleFormModal;
