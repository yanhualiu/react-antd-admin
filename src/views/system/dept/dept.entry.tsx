import cssModule from "@/views/system/dept/dept.module.scss";
import { Button, Table, message, Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import DeptSearch from "./searchForm";
import {
    getDeptTree,
    updateDept,
    addDept,
    deleteDeptById,
} from "../../../api/dept/index";
import { Dept } from "@/types";
import DeptFormModal from "./deptForm";
import { DeptForm } from "../../../types/system/index";
const DeptPage: React.FC = () => {
    const columns = [
        {
            title: "部门名称",
            dataIndex: "name",
            key: "name",
            width: "300px",
        },
        {
            title: "部门状态",
            dataIndex: "dataStatus",
            key: "dataStatus",
            render(dataStatus: number) {
                return <span>{dataStatus == 1 ? "启用" : "停用"}</span>;
            },
        },
        {
            title: "部门排序",
            dataIndex: "sort",
            key: "sort",
        },
        {
            title: "操作",
            key: "handle",
            width: "200px",
            render(row: Dept) {
                return (
                    <>
                        <Button
                            type="link"
                            size="small"
                            onClick={() => {
                                setParentId(row.id);
                                setVisible(true);
                            }}
                        >
                            新建
                        </Button>
                        <Button
                            type="link"
                            size="small"
                            onClick={() => {
                                setEditForm(row as any);
                                setVisible(true);
                            }}
                        >
                            编辑
                        </Button>
                        <Popconfirm
                            title="是否要删除当前部门"
                            onConfirm={() => deleteDept(row.id)}
                            okText="确定"
                            cancelText="取消"
                        >
                            <Button type="link" size="small">
                                删除
                            </Button>
                        </Popconfirm>
                    </>
                );
            },
        },
    ];
    const [deptTree, setDeptTree] = useState<Dept[]>([]);
    const [loading, setLoading] = useState(false);
    const searchForm = { value: {} };
    const getDeptList = () => {
        setLoading(true);
        getDeptTree(searchForm.value as any).then(res => {
            setLoading(false);
            setDeptTree(res.data);
        });
    };
    const [visible, setVisible] = useState(false);
    const [parentId, setParentId] = useState(0);
    const [editForm, setEditForm] = useState<DeptForm | null>(null);
    let submitFlag = false;
    const resetSubmitFlag = () => {
        submitFlag = false;
    };
    const submit = (form: DeptForm) => {
        if (submitFlag) {
            return;
        }
        submitFlag = true;
        if (editForm) {
            updateDept(form)
                .then(() => {
                    message.success("更新成功");
                    resetSubmitFlag();
                    getDeptList();
                    setVisible(false);
                })
                .catch(resetSubmitFlag);
        } else {
            addDept(form)
                .then(() => {
                    message.success("添加成功");
                    resetSubmitFlag();
                    getDeptList();
                    setVisible(false);
                })
                .catch(resetSubmitFlag);
        }
    };
    const handlerModalClose = () => {
        setVisible(false);
        setParentId(0);
        setEditForm(null);
    };
    const deleteDept = (id: number) => {
        deleteDeptById(id).then(() => {
            message.success("删除部门成功");
            getDeptList();
        });
    };
    useEffect(() => {
        getDeptList();
    }, []);
    return (
        <div className={cssModule.deptContainer}>
            <DeptSearch
                onSearch={v => {
                    searchForm.value = v;
                    getDeptList();
                }}
                onRest={() => {
                    searchForm.value = {};
                    getDeptList();
                }}
            ></DeptSearch>
            <div className={cssModule.btnWrap}>
                <Button type="default" onClick={() => setVisible(true)}>
                    新建
                </Button>
            </div>
            <div className="table-wrap">
                <Table
                    bordered
                    columns={columns}
                    loading={loading}
                    rowKey="id"
                    dataSource={deptTree}
                ></Table>
            </div>
            <DeptFormModal
                submit={submit}
                editForm={editForm}
                onCancel={handlerModalClose}
                closeModal={handlerModalClose}
                visible={visible}
                deptTree={deptTree}
                parentId={parentId}
            ></DeptFormModal>
        </div>
    );
};
export default DeptPage;
