import { Dept, DeptForm } from "@/types";
import { Modal, Form, TreeSelect, Input, InputNumber } from "antd";
import React, { useState, useEffect } from "react";
interface DeptFormProps {
    visible: boolean;
    onCancel: () => void;
    closeModal: () => void;
    deptTree: Dept[];
    parentId: number;
    editForm: DeptForm | null;
    submit: (form: DeptForm) => void;
}
const buildTreeNodes = (depts: Dept[]) => {
    return depts.map(d => {
        return (
            <TreeSelect.TreeNode value={d.id} key={d.id} title={d.name}>
                {d.children && buildTreeNodes(d.children)}
            </TreeSelect.TreeNode>
        );
    });
};
const DeptFormModal: React.FC<DeptFormProps> = props => {
    const initForm = () => {
        return {
            name: "",
            sort: 0,
            parentId: props.parentId,
        };
    };
    const [deptForm, setDeptForm] = useState<DeptForm>(initForm());
    const [form] = Form.useForm();
    const onOk = async () => {
        await form.validateFields();
        const values = form.getFieldsValue();
        values.id = props.editForm?.id;
        props.submit(values);
    };
    useEffect(() => {
        if (!props.visible) {
            setDeptForm(initForm());
        } else {
            setDeptForm(props.editForm ?? initForm());
            form.setFieldsValue(props.editForm ?? initForm());
        }
    }, [props.visible]);
    return (
        <Modal
            title="部门表单"
            onOk={onOk}
            visible={props.visible}
            onCancel={props.onCancel}
        >
            <Form form={form}>
                <Form.Item
                    label="上级部门"
                    name="parentId"
                    rules={[{ required: true, message: "请选择上级部门" }]}
                >
                    <TreeSelect
                        showSearch
                        value={deptForm.parentId}
                        onChange={v =>
                            setDeptForm({ ...deptForm, parentId: v })
                        }
                    >
                        {buildTreeNodes(props.deptTree)}
                    </TreeSelect>
                </Form.Item>
                <Form.Item
                    label="部门名称"
                    name="name"
                    rules={[{ required: true, message: "请填写部门名称" }]}
                >
                    <Input
                        value={deptForm.name}
                        onChange={e =>
                            setDeptForm({ ...deptForm, name: e.target.value })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item
                    label="部门排序"
                    name="sort"
                    rules={[{ required: true, message: "请填写序号" }]}
                >
                    <InputNumber
                        value={deptForm.sort}
                        min={0}
                        onChange={v => setDeptForm({ ...deptForm, sort: v })}
                    ></InputNumber>
                </Form.Item>
            </Form>
        </Modal>
    );
};
export default DeptFormModal;
