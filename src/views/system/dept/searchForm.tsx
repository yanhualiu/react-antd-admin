import React from "react";
import { Button, Form, Input, Select } from "antd";
import { dataStatusOptions } from "../../../utils/utils";
import { useState } from "react";
interface SearchForm {
    deptName: string;
    dataStatus?: number;
}
interface SearchProps {
    onSearch: (form: SearchForm) => void;
    onRest: () => void;
}
const DeptSearch: React.FC<SearchProps> = props => {
    const [searchForm, setSearchForm] = useState<SearchForm>({
        deptName: "",
        dataStatus: undefined,
    });
    return (
        <div className="search-wrap">
            <Form layout="inline" onFinish={() => props.onSearch(searchForm)}>
                <Form.Item label="部门名称">
                    <Input
                        value={searchForm.deptName}
                        onChange={e =>
                            setSearchForm({
                                ...searchForm,
                                deptName: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item label="状态">
                    <Select
                        placeholder="请选择状态"
                        style={{ width: 150 }}
                        value={searchForm.dataStatus}
                        onChange={v =>
                            setSearchForm({
                                ...searchForm,
                                dataStatus: v as any,
                            })
                        }
                        allowClear
                    >
                        {dataStatusOptions.map(d => (
                            <Select.Option key={d.value} value={d.value}>
                                {d.label}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        搜索
                    </Button>
                </Form.Item>
                <Form.Item>
                    <Button
                        type="default"
                        htmlType="reset"
                        onClick={() => {
                            setSearchForm({
                                deptName: "",
                                dataStatus: undefined,
                            });
                            props.onRest();
                        }}
                    >
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};
export default DeptSearch;
