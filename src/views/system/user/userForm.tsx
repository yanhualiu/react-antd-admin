import { Modal, Form, Input, Select, TreeSelect } from "antd";
import React, { useState, useEffect } from "react";
import { UserForm, Dept, Role } from "@/types";
interface Props {
    visible: boolean;
    deptTree: Dept[];
    onCancel: () => void;
    roleList: Role[];
    deptId: number;
    editForm: UserForm | null;
    submit: (userForm: UserForm) => void;
}
const buildTreeNodes = (depts: Dept[]) => {
    return depts.map(d => {
        return (
            <TreeSelect.TreeNode value={d.id} key={d.id} title={d.name}>
                {d.children && buildTreeNodes(d.children)}
            </TreeSelect.TreeNode>
        );
    });
};
const UserFormModal: React.FC<Props> = props => {
    const [userForm, setUserForm] = useState<UserForm>({} as UserForm);
    const [form] = Form.useForm();
    const onOk = async () => {
        await form.validateFields();
        const values = form.getFieldsValue();
        values.id = props.editForm?.id;
        props.submit(values);
    };
    useEffect(() => {
        if (props.visible) {
            const newForm = { ...userForm, deptId: props.deptId };
            setUserForm(props.editForm ?? newForm);
            form.setFieldsValue(props.editForm ?? newForm);
        }
    }, [props.visible]);
    return (
        <Modal
            visible={props.visible}
            onCancel={props.onCancel}
            onOk={onOk}
            title="用户表单"
        >
            <Form form={form} labelCol={{ span: 4 }}>
                <Form.Item
                    name="deptId"
                    label="部门"
                    rules={[{ required: true, message: "请选择部门" }]}
                >
                    <TreeSelect
                        value={userForm.deptId}
                        onChange={v => setUserForm({ ...userForm, deptId: v })}
                    >
                        {buildTreeNodes(props.deptTree)}
                    </TreeSelect>
                </Form.Item>
                <Form.Item
                    name="username"
                    label="用户名"
                    key="username"
                    rules={[{ required: true, message: "请填写用户名" }]}
                >
                    <Input
                        value={userForm.username}
                        onChange={e =>
                            setUserForm({
                                ...userForm,
                                username: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item
                    name="nickname"
                    label="用户名称"
                    key="nickname"
                    rules={[{ required: true, message: "请填写用户名称" }]}
                >
                    <Input
                        value={userForm.nickname}
                        onChange={e =>
                            setUserForm({
                                ...userForm,
                                nickname: e.target.value,
                            })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item
                    name="phone"
                    label="手机号"
                    key="phone"
                    rules={[{ required: true, message: "请填写手机号" }]}
                >
                    <Input
                        value={userForm.phone}
                        onChange={e =>
                            setUserForm({ ...userForm, phone: e.target.value })
                        }
                    ></Input>
                </Form.Item>
                <Form.Item name="roles" label="关联角色" key="roles">
                    <Select
                        mode="multiple"
                        value={userForm.roles}
                        onChange={v => setUserForm({ ...userForm, roles: v })}
                    >
                        {props.roleList.map(r => {
                            return (
                                <Select.Option key={r.id} value={r.id}>
                                    {r.roleDesc}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
};
export default UserFormModal;
