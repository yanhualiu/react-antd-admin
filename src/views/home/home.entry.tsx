import { Card, Table } from "antd";
import cssModule from "@/views/home/home.module.scss";
import usePageOption from "../../hooks/usePageOption";
import { useEffect, useState, useRef } from "react";
import { getLoginLog } from "../../api/login/index";
import { LoginLog } from "../../types/login/index";
export default Home;
function Home() {
    const [pageConfig, setPageConfig] = useState({ page: 1, pageSize: 10 });
    const [pageOption, setPageOption] = usePageOption((page, pageSize) => {
        setPageConfig({
            page,
            pageSize: pageSize ? pageSize : pageConfig.pageSize,
        });
    });
    const columns = [
        {
            title: "账号",
            dataIndex: "username",
            key: "username",
            width: "100px",
        },
        {
            title: "昵称",
            dataIndex: "nickname",
            key: "nickname",
            width: "150px",
        },
        {
            title: "ip",
            dataIndex: "ip",
            key: "ip",
            width: "150px",
        },
        {
            title: "登录地点",
            dataIndex: "location",
            key: "location",
            width: "150px",
        },
        {
            title: "操作系统",
            dataIndex: "os",
            key: "os",
        },
        {
            title: "浏览器",
            dataIndex: "browser",
            key: "browser",
        },
        {
            title: "登录时间",
            dataIndex: "loginTime",
            key: "loginTime",
        },
    ];
    const [data, setData] = useState<LoginLog[]>([]);
    const [scroll, setScroll] = useState({ y: 100 });
    const ref = useRef<HTMLDivElement>(null);
    const getLoginLogList = () => {
        getLoginLog(pageConfig).then(res => {
            setPageOption({ ...pageOption, total: res.data.total });
            setData(res.data.list);
        });
    };
    useEffect(() => {
        getLoginLogList();
        const y = ref.current?.clientHeight! - 200;
        setScroll({ y });
    }, [pageConfig]);
    return (
        <div ref={ref}>
            <Card hoverable>
                <h1>React-Antd-Admin</h1>
                <div className={cssModule.details}>
                    <h2>前端采用：React+React Router + Redux + Antd +Vite</h2>
                    <h2>后端采用：NodeJs + NestJs + TypeOrm + Mysql</h2>
                </div>
                <Table
                    pagination={pageOption}
                    columns={columns}
                    rowKey="id"
                    dataSource={data}
                    scroll={scroll}
                ></Table>
            </Card>
        </div>
    );
}
