import React, { useEffect } from "react";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import cssModule from ".//layout.module.scss";
import Aside from "./aside/aside";
import Main from "./main/main";
import { useMySelector } from "../../redux/hook";
import { useDispatch } from "react-redux";
import { toggleCollapseAction } from "../../redux/menus/menuReducerAction";
import Header from "./header/header";
import TagView from "./tags/tagView";
import { useLocation } from "react-router-dom";
import useHistory from "../../hooks/useHistory";
import { getToken } from "@/utils/token";
import { getUserAction } from "../../redux/user/userReducer";

const Layout: React.FC = props => {
    const collapse = useMySelector(state => state.menu.collapse);
    const location = useLocation();
    const history = useHistory();
    const dispatch = useDispatch();
    const toggleCollapse = () => {
        dispatch(toggleCollapseAction(!collapse));
    };
    history.beforeEach((form, to, next) => {
        const path = to.pathname;
        const token = getToken();
        if (!token && path !== "/login") {
            next({ pathname: "/login" });
        } else if (token && path === "/login") {
            next({ pathname: "/home" });
        } else {
            next();
        }
    });
    useEffect(() => {
        if (location.pathname !== "/login") {
            dispatch(getUserAction());
        }
    }, [location]);
    return location.pathname === "/login" ? (
        <div />
    ) : (
        <div className={cssModule.container}>
            <Aside />

            <Main>
                <Header onClick={toggleCollapse} />
                {props.children && <TagView />}
                <div
                    className={cssModule.mainWrapper}
                    style={{ padding: "10px" }}
                >
                    {props.children}
                </div>
            </Main>
        </div>
    );
};
export default Layout;
