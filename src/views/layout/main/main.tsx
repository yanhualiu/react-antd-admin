import React from "react";
const Main: React.FC = props => {
    return <main>{props.children}</main>;
};
export default Main;
