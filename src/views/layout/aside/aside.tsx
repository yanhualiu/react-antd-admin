import cssModule from "@/views/layout/layout.module.scss";
import React from "react";
import { Menu } from "antd";
import { useMySelector } from "../../../redux/hook";
import Icon from "../../../components/icon";
import { IRoute } from "@/types";
import { useLocation } from "react-router";
import useHistory from "../../../hooks/useHistory";
const { SubMenu } = Menu;
const submenu = (menus: IRoute[]) => {
    return menus.map(m => {
        if (m.children && m.children.length > 0) {
            return (
                <SubMenu
                    key={m.path}
                    icon={<Icon className={m.meta?.icon}></Icon>}
                    title={m.meta?.title}
                >
                    {submenu(m.children)}
                </SubMenu>
            );
        } else {
            return (
                <Menu.Item
                    key={m.path}
                    icon={<Icon className={m.meta?.icon}></Icon>}
                >
                    {m.meta?.title}
                </Menu.Item>
            );
        }
    });
};
const Aside: React.FC = () => {
    const menus = useMySelector(state => state.menu.menus);
    const collapse = useMySelector(state => state.menu.collapse);
    const history = useHistory();
    const location = useLocation();
    const menuItemClickHandler = (args: any) => {
        history.push({ pathname: args.key });
    };
    return (
        <aside className={collapse ? cssModule.collapse : ""}>
            <Menu
                defaultOpenKeys={["/system"]}
                defaultSelectedKeys={[location.pathname]}
                key={location.pathname}
                mode="inline"
                theme="dark"
                inlineCollapsed={collapse}
                onClick={menuItemClickHandler}
            >
                {submenu(menus)}
            </Menu>
        </aside>
    );
};

export default Aside;
