import cssModule from "@/views/layout/layout.module.scss";
import { Tag } from "@/types";
import { Menu } from "antd";
export type EventType = "this" | "other" | "all";
interface PropsType {
    tag: Tag;
    position: { left: number; top: number };
    menuClick: (type: EventType) => void;
}
const ContextMenu = ({ tag, position, menuClick }: PropsType) => {
    return (
        <div className={cssModule.contextMenu} style={position}>
            <Menu
                onClick={({ key }) => {
                    menuClick(key as EventType);
                }}
            >
                {!tag.isAffix && <Menu.Item key="this">关闭当前</Menu.Item>}
                <Menu.Item key="other">关闭其他</Menu.Item>
                <Menu.Item key="all">关闭所有</Menu.Item>
            </Menu>
        </div>
    );
};
export default ContextMenu;
