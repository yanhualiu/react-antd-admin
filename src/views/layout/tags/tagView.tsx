import React, { useState, useEffect, MouseEvent, FocusEvent } from "react";
import cssModule from "@/views/layout/layout.module.scss";
import BetterScroll, { BScrollInstance } from "@better-scroll/core";
import { CloseOutlined } from "@ant-design/icons";
import { useMySelector } from "../../../redux/hook";
import { Tag } from "@/types";
import { useDispatch } from "react-redux";
import { addTag, delTag } from "../../../redux/tagView/tagViewReducer";
import ContextMenu, { EventType } from "@/views/layout/tags/contextMenu";
import { useLocation } from "react-router";
import useHistory from "../../../hooks/useHistory";
let bscroll: BScrollInstance;
let isLast = false;
let closeIndex = -1;
const TagView: React.FC = () => {
    const tags = useMySelector(state => state.tagViews.visibleTags);
    const dispatch = useDispatch();
    const [visible, setVisible] = useState(false);
    const [curTag, setCurTag] = useState<Tag | null>(null);
    const [curIndex, setCurIndex] = useState(-1);
    const [position, setPosition] = useState({
        left: 0,
        top: 0,
    });
    const history = useHistory();
    const location = useLocation();
    useEffect(() => {
        dispatch(addTag(location.pathname));
    }, [location]);
    const initScroll = () => {
        const wrapper = document.querySelector(
            `.${cssModule.tagViewContainer}`
        );
        bscroll = new BetterScroll(wrapper as any, {
            scrollX: true,
            scrollY: false,
        });
    };
    const closeTag = (e: MouseEvent, tag: Tag, index: number) => {
        e.stopPropagation();
        const nTags = [...tags];
        isLast = index === nTags.length - 1;
        closeIndex = index;
        nTags.splice(index, 1);
        if (location.pathname === tag.path) {
            const activeTag = nTags[closeIndex]
                ? nTags[closeIndex]
                : nTags[closeIndex - 1];
            if (activeTag) {
                history.push({ pathname: activeTag.path });
            }
        }
        dispatch(delTag(nTags));
    };
    const openContextMenu = (e: MouseEvent, tag: Tag, index: number) => {
        e.preventDefault();
        setVisible(true);
        setCurTag(tag);
        setCurIndex(index);
        const docClientWidth = document.body.clientWidth;
        const menuWidth = 95;
        let left = e.clientX;
        let top = e.clientY;
        if (e.clientX + menuWidth > docClientWidth) {
            left = docClientWidth - menuWidth - 20;
        }
        setPosition({ left, top });
    };
    const tagBlur = (e: FocusEvent<HTMLLIElement>) => {
        setTimeout(() => {
            setVisible(false);
        }, 200);
    };
    const tagClick = (e: MouseEvent, tag: Tag) => {
        bscroll.scrollToElement(e.target as HTMLLIElement, 200, true, 0);
        setVisible(false);
        history.push({ pathname: tag.path });
    };
    const menuClick = (key: EventType) => {
        let newTags = [...tags];
        if (key === "this") {
            closeIndex = curIndex;
            newTags.splice(curIndex, 1);
            const activeTag = newTags[curIndex]
                ? newTags[curIndex]
                : newTags[curIndex - 1];
            if (activeTag) {
                history.push({ pathname: activeTag.path });
            }
        } else if (key === "other") {
            newTags = tags.filter(t => t.isAffix || t.path === curTag?.path);
            closeIndex = newTags.length;
        } else if (key === "all") {
            newTags = tags.filter(t => t.isAffix);
            closeIndex = newTags.length;
            const activeTag = newTags[newTags.length - 1];
            history.push({ pathname: activeTag.path });
        }
        dispatch(delTag(newTags));
    };
    useEffect(() => {
        if (tags.length === 0) {
            return;
        }
        const timer = setTimeout(() => {
            if (bscroll) {
                bscroll.refresh();
                closeIndex > 0 &&
                    bscroll.scrollToElement(
                        `.${cssModule.tagWrap} > li:nth-of-type(${closeIndex})`,
                        200,
                        true,
                        0
                    );
                return;
            }
            initScroll();
        }, 200);
        return () => {
            clearTimeout(timer);
        };
    }, [tags]);
    return (
        <>
            <nav>
                <div className={cssModule.tagViewContainer}>
                    <ul className={cssModule.tagWrap}>
                        {tags.map((t, index) => (
                            <li
                                className={`${cssModule.tagItem} ${
                                    location.pathname === t.path
                                        ? cssModule.active
                                        : ""
                                }`}
                                tabIndex={(index + 1) * 100}
                                onContextMenu={e =>
                                    openContextMenu(e, t, index)
                                }
                                onClick={e => tagClick(e, t)}
                                onBlur={e => tagBlur(e)}
                                key={t.name}
                            >
                                <span>{t.name}</span>
                                {!t.isAffix && (
                                    <span onClick={e => closeTag(e, t, index)}>
                                        <CloseOutlined
                                            className={cssModule.tagCloseIcon}
                                        />
                                    </span>
                                )}
                            </li>
                        ))}
                    </ul>
                </div>
            </nav>
            {visible && (
                <ContextMenu
                    tag={curTag!}
                    position={position}
                    menuClick={menuClick}
                />
            )}
        </>
    );
};
export default TagView;
