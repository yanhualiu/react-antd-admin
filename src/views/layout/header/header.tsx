import React from "react";
import cssModule from "@/views/layout/layout.module.scss";
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    DownOutlined,
} from "@ant-design/icons";
import { useMySelector } from "../../../redux/hook";
import { Avatar, Dropdown, Menu, message } from "antd";
import { useHistory } from "react-router";
import { loginOut } from "../../../api/login/index";
import { removeToken } from "../../../utils/token";
interface HeaderProps {
    onClick: () => void;
}
const Header: React.FC<HeaderProps> = ({ onClick }) => {
    const collapse = useMySelector(state => state.menu.collapse);
    const userInfo = useMySelector(state => state.user.userInfo);
    const history = useHistory();
    const dropdownItem = (
        <Menu
            onClick={data => {
                if (data.key == "2") {
                    loginOut().then(() => {
                        removeToken();
                        message.success("登出成功");
                        setTimeout(() => {
                            location.href = "/login";
                        }, 500);
                    });
                    return;
                }
                history.push({ pathname: data.key });
            }}
        >
            <Menu.Item key="/system/user-center">用户中心</Menu.Item>
            <Menu.Item key="2">退出</Menu.Item>
        </Menu>
    );
    return (
        <header>
            <span className={cssModule.leftIcon} onClick={onClick}>
                {!collapse ? (
                    <MenuFoldOutlined style={{ fontSize: "20px" }} />
                ) : (
                    <MenuUnfoldOutlined style={{ fontSize: "20px" }} />
                )}
            </span>
            <div className={cssModule.rightUserWrap}>
                <Avatar
                    size={35}
                    src={userInfo?.avatar}
                    icon={<UserOutlined />}
                />
                <Dropdown overlay={dropdownItem}>
                    <span
                        className="ant-dropdown-link"
                        onClick={e => e.preventDefault()}
                    >
                        {userInfo?.nickname}
                        <DownOutlined />
                    </span>
                </Dropdown>
            </div>
        </header>
    );
};
export default Header;
