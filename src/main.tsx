import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { ConfigProvider } from "antd";
import zhCN from "antd/lib/locale/zh_CN";
import moment from "moment";
import "moment/locale/zh-cn";
import "./assets/font/iconfont.css";
import "./style/global.scss";
import "./index.css";
import store from "./redux/index";
moment.locale("zh-cn");
ReactDOM.render(
    <Provider store={store}>
        <ConfigProvider locale={zhCN}>
            <App></App>
        </ConfigProvider>
    </Provider>,
    document.getElementById("root")
);
