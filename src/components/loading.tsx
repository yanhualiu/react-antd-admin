import { useEffect } from "react";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
const Loading: React.FC<{}> = () => {
    NProgress.start();
    useEffect(() => {
        return () => {
            NProgress.done();
        };
    }, []);
    return null;
};
export default Loading;
