const Icon = (props: { className: string }) => {
    return <i className={`iconfont ${props.className}`}></i>;
};
export default Icon;
