export interface Tag {
    name: string;
    path: string;
    isAffix?: boolean;
}
