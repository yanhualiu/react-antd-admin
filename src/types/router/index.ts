export interface IRoute {
    name: string;
    path?: string;
    auth?: "auth" | "noAuth";
    component?: any;
    exact?: true;
    children?: IRoute[];
    redirect?: { path: string; to: string; exact?: true };
    meta?: { [key in string]: any };
}
