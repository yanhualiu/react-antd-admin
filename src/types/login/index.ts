export interface ImgCode {
    key: string;
    svg: string;
}
export interface LoginForm {
    username: string;
    password: string;
    code: string;
    codeKey: string;
}
export interface LoginRes {
    token: string;
}

export interface LoginLog {
    id: number;
    os: string;
    browser: string;
    ip: string;
    location: string;
    ua: string;
    loginTime: string;
    userId: number;
    username: string;
    nickname: string;
}
