export interface Menu {
    id: number;
    name: string;
    permission: string;
    path: string;
    parentId: number;
    icon: string;
    component: string;
    type: number;
    sort: number;
    dataStatus: number;
    layout: boolean;
    scope: string;
    hidden: boolean;
    createTime: string;
    updateTime: string;
    children?: Menu[];
}
export interface MenuForm {
    id?: number;
    name: string;
    permission?: string;
    path: string;
    parentId?: number;
    icon?: string;
    component: string;
    type?: number;
    sort: number;
    layout?: boolean;
    scope?: string;
    hidden?: boolean;
}

export interface RoleForm {
    id?: number;
    roleName: string;
    roleDesc: string;
    menus: number[];
}
export interface Role {
    id: number;
    roleName: string;
    roleDesc: string;
    dataStatus: boolean;
    createTime: string;
    updateTime: string;
    menus?: Menu[];
}
export interface Dept {
    id: number;
    parentId: number;
    name: string;
    ancestors: string;
    sort: number;
    dataStatus: number;
    createTime: string;
    updateTime: string;
    children?: Dept[];
}
export interface DeptForm {
    id?: number;
    parentId: number;
    name: string;
    sort: number;
}
export interface User {
    id: number;
    username: string;
    nickname: string;
    phone: string;
    avatar: string;
    dataStatus: number;
    createTime: string;
    updateTime: string;
    dept: Dept;
    roles: Role[];
}
export interface UserForm {
    id?: number;
    username: string;
    nickname: string;
    phone: string;
    avatar: string;
    deptId?: number;
    roles?: number[];
}
