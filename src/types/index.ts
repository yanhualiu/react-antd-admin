export * from "./system";
export * from "./router/index";
export * from "./tagView/index";
export * from "./login";

export interface Page<T> {
    currentPage: number;
    list: T[];
    pageSize: number;
    total: number;
    totalPage: number;
}
export interface FileData {
    createTime: string;
    updateTime: string;
    name: string;
    fileName: string;
    path: string;
    id: number;
}
