import { RoleForm, Role, Page } from "@/types";
import request from "@/utils/request";
export const addRole = (roleForm: RoleForm) => {
    return request.post(`/role`, roleForm);
};
export const updateRole = (roleForm: RoleForm) => {
    return request.put(`/role`, roleForm);
};
export const getRoleById = (id: number) => {
    return request.get<Role>(`/role/${id}`);
};
export const getRoleListByPage = (params: {
    roleName?: string;
    roleDesc?: string;
    dataStatus?: number;
    createTimeRange?: string[];
    page: number;
    pageSize: number;
}) => {
    return request.get<Page<Role>>(`/role/list`, { params });
};
export const deleteRoleById = (id: number) => {
    return request.delete(`/role/${id}`);
};
export const getAllRoleList = () => {
    return request.get<Role[]>("/role");
};
