import request from "@/utils/request";
import { FileData } from "@/types";
export const uploadFile = (file: Blob | File, fileName?: string) => {
    const formData = new FormData();
    formData.append("file", file);
    return request.post<FileData>(`/file/upload`, formData, {
        params: { fileName },
    });
};
