import request from "@/utils/request";
import { Page, User, UserForm } from "@/types";
export const getUserListByPage = (params: any) => {
    return request.get<Page<User>>("/user/list", { params });
};
export const getUserById = (id: number) => {
    return request.get<User>(`/user/${id}`);
};
export const addUser = (form: UserForm) => {
    return request.post("/user", form);
};
export const updateUser = (form: UserForm) => {
    return request.put("/user", form);
};
export const deleteUser = (id: number) => {
    return request.delete(`/user/${id}`);
};
export const getUserInfo = () => {
    return request.get<User>("/user/userInfo");
};
export const changeUserInfo = (form: UserForm) => {
    return request.put("/user/changeUserBasicInfo", form);
};
export const changePassword = (data: any) => {
    return request.put("/user/changePassword", data);
};
