import request, { Response } from "@/utils/request";
import { ImgCode, LoginForm, LoginRes, Page } from "@/types";
import { LoginLog } from "@/types/login/index";

export const getImgCode = () => {
    return request<ImgCode>("/captcha");
};
export const login = (loginForm: LoginForm) => {
    return request.post<LoginRes>("/login", loginForm);
};
export const loginOut = () => {
    return request.post("/loginOut");
};
export const getLoginLog = (data: any) => {
    return request.get<Page<LoginLog>>("/loginLog", { params: data });
};
