import request from "@/utils/request";
import { Menu, MenuForm } from "@/types";
import { IRoute } from "../../types/router/index";

export const getMenuTree = (params?: { menuName?: string }) => {
    return request.get<Menu[]>("/menu/tree", { params });
};
export const addMenu = (form: MenuForm) => {
    return request.post<Menu>("/menu", form);
};
export const updateMenu = (form: MenuForm) => {
    return request.put<Menu>("/menu", form);
};
export const delMenu = (id: number) => {
    return request.delete<Menu>(`/menu/${id}`);
};
export const getUserMenu = () => {
    return request.get<IRoute[]>("/menu/routers");
};
