import { Dept } from "@/types";
import request from "@/utils/request";
import { DeptForm } from "../../types/system/index";
export const getDeptTree = (params?: {
    deptName: string;
    dataStatus: number;
}) => {
    return request.get<Dept[]>("/dept/tree", { params });
};
export const getDeptById = (id: number) => {
    return request.get<Dept>(`/dept/${id}`);
};
export const deleteDeptById = (id: number) => {
    return request.delete(`/dept/${id}`);
};
export const addDept = (form: DeptForm) => {
    return request.post("/dept", form);
};
export const updateDept = (form: DeptForm) => {
    return request.put("/dept", form);
};
