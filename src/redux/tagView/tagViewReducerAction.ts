import { Tag } from "../../types/tagView/index";
export const CHANGE_VISIBLE_TAGS = "change_visible_tags";
export const CHANGE_ACTIVE_PATH = "change_active_path";
export interface ChangeVisibleTagsAction {
    type: typeof CHANGE_VISIBLE_TAGS;
    payload: Tag[];
}
export type TagViewAction = ChangeVisibleTagsAction;
export const changeVisibleTagsActionCreator = (tags: Tag[]): TagViewAction => {
    return {
        type: CHANGE_VISIBLE_TAGS,
        payload: tags,
    };
};
