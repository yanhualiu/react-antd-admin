import { User } from "@/types";
export const SET_TOKEN = "set_token";
export const SET_USER_INFO = "set_user_info";
interface SetTokenAction {
    type: typeof SET_TOKEN;
    payload: string;
}
interface SetUserInfoAction {
    type: typeof SET_USER_INFO;
    payload: User;
}
export type UserAction = SetTokenAction | SetUserInfoAction;
export const setTokenActionCreator = (token: string): SetTokenAction => {
    return {
        type: SET_TOKEN,
        payload: token,
    };
};
export const setUserInfoActionCreator = (user: User): SetUserInfoAction => {
    return {
        type: SET_USER_INFO,
        payload: user,
    };
};
