import { LoadableClassComponent } from "@loadable/component";
import loadable from "@loadable/component";
import { lazy } from "react";
import { IRoute } from "../../types/router/index";
import {
    MenuAction,
    SET_MENU,
    SET_ROUTE,
    setRouteActionCreator,
    setMenuActionCreator,
    TOGGLE_COLLAPSE,
} from "./menuReducerAction";
import _ from "lodash";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../index";
import { Tag } from "@/types";
import { Routers } from "@/router";
import { getToken } from "../../utils/token";
import { getUserMenu } from "../../api/menu/index";
import { generateAffixTag } from "../tagView/tagViewReducer";
import Loading from "../../components/loading";
const modules = import.meta.glob("../../views/**/*.entry.tsx");
console.log(modules);
interface MenuState {
    menus: IRoute[];
    routes: IRoute[];
    collapse: boolean;
}
const menuState: MenuState = {
    menus: [],
    routes: [],
    collapse: false,
};
export const generateRoutes = (): ThunkAction<
    void,
    RootState,
    unknown,
    MenuAction
> => {
    return async dispatch => {
        let remoteRouters: IRoute[] = [];
        if (getToken()) {
            const res = await getUserMenu();
            remoteRouters = res.data;
        }
        const routes = Routers.concat(remoteRouters).concat(
            remoteRouters.length === 0
                ? [
                      {
                          name: "RedirectLogin",
                          redirect: { path: "/", to: "/login", exact: true },
                      },
                  ]
                : [
                      {
                          name: "RedirectHome",
                          redirect: { path: "/", to: "/home", exact: true },
                      },
                  ]
        );
        const tRoutes = translateComponent(routes);
        dispatch(setRouteActionCreator(tRoutes));
        const menus = generateMenus(routes);
        dispatch(setMenuActionCreator(menus));
        dispatch(generateAffixTag(routes));
    };
};
const generateMenus = (menus: IRoute[]) => {
    menus = _.cloneDeep(menus);
    const buildMenus = (m: IRoute[]) => {
        for (let i = 0; i < m.length; i++) {
            const menu = m[i];
            if (menu.meta?.hidden || menu.redirect) {
                m.splice(i, 1);
                i--;
            }
            if (menu.children) {
                buildMenus(menu.children);
            }
        }
    };
    buildMenus(menus);
    return menus;
};
const translateComponent = (routes: IRoute[]) => {
    routes = _.cloneDeep(routes);
    const buildRoutes = (r: IRoute[]) => {
        r.forEach(e => {
            if (typeof e.component === "string") {
                e.component = loadComponent(e.component);
            }
            if (e.children) {
                buildRoutes(e.children);
            }
        });
    };
    buildRoutes(routes);
    return routes;
};
const loadComponent = (component: string) => {
    return loadable(
        import.meta.env.DEV
            ? () => import(`../../views/${component}.entry.tsx`)
            : (modules[`../../views/${component}.entry.tsx`] as any),
        { fallback: <Loading></Loading> }
    );
};
export default (state = menuState, action: MenuAction) => {
    switch (action.type) {
        case SET_MENU:
            return { ...state, menus: action.payload };
        case SET_ROUTE:
            return { ...state, routes: action.payload };
        case TOGGLE_COLLAPSE:
            return { ...state, collapse: action.payload };
        default:
            return state;
    }
};
