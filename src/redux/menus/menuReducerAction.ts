import { IRoute } from "../../types/router/index";
export const SET_MENU = "set_menu";
export const SET_ROUTE = "set_route";
export const TOGGLE_COLLAPSE = "toggle_collapse";
export interface SetMenuAction {
    type: typeof SET_MENU;
    payload: IRoute[];
}
export interface SetRouteAction {
    type: typeof SET_ROUTE;
    payload: IRoute[];
}
export interface ToggleCollapseAction {
    type: typeof TOGGLE_COLLAPSE;
    payload: boolean;
}
export type MenuAction = SetMenuAction | SetRouteAction | ToggleCollapseAction;
export const setMenuActionCreator = (menus: IRoute[]): SetMenuAction => {
    return {
        type: SET_MENU,
        payload: menus,
    };
};
export const setRouteActionCreator = (routes: IRoute[]): SetRouteAction => {
    return {
        type: SET_ROUTE,
        payload: routes,
    };
};

export const toggleCollapseAction = (
    payload: boolean
): ToggleCollapseAction => {
    return {
        type: TOGGLE_COLLAPSE,
        payload,
    };
};
