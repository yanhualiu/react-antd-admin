import { useSelector, TypedUseSelectorHook } from "react-redux";
import { RootState } from "./index";
export const useMySelector: TypedUseSelectorHook<RootState> = useSelector;
