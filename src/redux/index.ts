import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import menuReducer from "./menus/menuReducer";
import tagViewReducer from "./tagView/tagViewReducer";
import userReducer from "@/redux/user/userReducer";
const store = createStore(
    combineReducers({
        menu: menuReducer,
        tagViews: tagViewReducer,
        user: userReducer,
    }),
    applyMiddleware(thunk)
);
export type RootState = ReturnType<typeof store.getState>;
export default store;
